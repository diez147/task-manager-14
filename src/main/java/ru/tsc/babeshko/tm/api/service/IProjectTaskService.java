package ru.tsc.babeshko.tm.api.service;

public interface IProjectTaskService {

    void bindTaskToProject(String projectId, String TaskId);

    void unbindTaskFromProject(String projectId, String TaskId);

    void removeProjectById(String projectId);

}
