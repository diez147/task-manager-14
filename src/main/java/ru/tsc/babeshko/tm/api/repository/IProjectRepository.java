package ru.tsc.babeshko.tm.api.repository;

import ru.tsc.babeshko.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    Project create(String name);

    List<Project> findAll();

    List<Project> findAll(Comparator comparator);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    boolean existsById(String id);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project create(String name, String description);

    void remove(Project project);

    Project add(Project project);

    void clear();

}
